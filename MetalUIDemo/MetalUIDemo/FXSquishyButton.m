//
//  FXSquishyButton.m
//  MetalUIDemo
//
//  Created by George Ostrobrod on 4/11/21.
//

#import <Metal/Metal.h>
#import <simd/simd.h>

#import "FXSquishyButton.h"
#import "MetalTools.h"



@implementation FXSquishyButton
{
	id<MTLDevice> _device;
	id<MTLCommandQueue> _queue;
	
	id<MTLRenderPipelineState> _pipelineButton;
	id<MTLRenderPipelineState> _pipelineDuDvUpdate;
	id<MTLRenderPipelineState> _pipelineDuDvDraw;
	
	CADisplayLink *_displayLink;
	
	id<MTLBuffer> _mesh;
	NSUInteger _meshSize;
	float _maxRadius;
	
	id<MTLTexture> _dudvMap;
	id<MTLTexture> _lizardTexture;
	
	BOOL _touchIsOut;
	float _crashTime;
	BOOL _touchBegan;
	simd_float2 _touchPosition;
}



+ (Class)layerClass {
	return CAMetalLayer.class;
}



- (void)didMoveToWindow {
	[super didMoveToWindow];
	
	if(self.window == nil) {
		// If moving off of a window destroy the display link.
		[_displayLink invalidate];
		_displayLink = nil;
		return;
	}
	
	[self setupCADisplayLinkForScreen:self.window.screen];
	[_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}


- (void)setupCADisplayLinkForScreen:(UIScreen*)screen {
	[_displayLink invalidate];
	_displayLink = [screen displayLinkWithTarget:self selector:@selector(render)];
	_displayLink.paused = NO;
	_displayLink.preferredFramesPerSecond = 60;
}



- (instancetype)initWithCoder:(NSCoder *)coder {
	self = [super initWithCoder:coder];
	
	CAMetalLayer *metalLayer = (CAMetalLayer*)self.layer;
	id<CAMetalDrawable> drawable = [metalLayer nextDrawable];
	
	_device = metalLayer.device;
	_queue = [_device newCommandQueue];
	
	_pipelineButton = buildRenderPipeline(_device,
										  drawable.texture.pixelFormat,
										  drawable.texture.sampleCount,
										  @"fshSquishyButton",
										  @"vshSquishyButton");
	_pipelineDuDvUpdate = buildRenderPipeline(_device,
											  MTLPixelFormatRG32Float,
											  1,
											  @"fshDudvMapUpdate",
											  @"vshDudvMapUpdate");
	_pipelineDuDvDraw = buildRenderPipeline(_device,
											MTLPixelFormatRG32Float,
											1,
											@"fshDudvMapDraw",
											@"vshDudvMapDraw");
	
	int W = (int)drawable.texture.width;
	int H = (int)drawable.texture.height;
	_maxRadius = MIN(32, MIN(W, H) / 3);
	_mesh = buildMesh(_device,
					  MTLSizeMake(W - 2 * _maxRadius, H - 2 * _maxRadius, 0),
					  MTLSizeMake(_maxRadius, _maxRadius, 0),
					  MIN(5, _maxRadius),
					  &_meshSize);
	_dudvMap = createTexture(_device, W, H, MTLPixelFormatRG32Float);
	
	_lizardTexture = loadTexture(_device, [UIImage imageNamed:@"skink"]);
	
	return self;
}



- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
	CGRect bounds = CGRectMake(_maxRadius,
							   _maxRadius,
							   self.frame.size.width - _maxRadius * 2,
							   self.frame.size.height - _maxRadius * 2);
	if (CGRectContainsPoint(bounds, point)) {
		return YES;
	}
	return NO;
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	_touchBegan = YES;
	CGPoint pos = [touches.anyObject locationInView:self];
	_touchPosition = simd_make_float2(pos.x, pos.y);
	
	[super touchesBegan:touches withEvent:event];
}


- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	CGPoint pos = [touches.anyObject locationInView:self];
	_touchPosition = simd_make_float2(pos.x, pos.y);
	if (self != [self hitTest:pos withEvent:event]) {
		_touchIsOut = YES;
	} else {
		_touchIsOut = NO;
		_crashTime = 0;
		
	}
	
	[super touchesMoved:touches withEvent:event];
}



- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	_touchBegan = NO;
	_touchIsOut = NO;
	_crashTime = 0;
	[super touchesEnded:touches withEvent:event];
}



- (void)render {	
	id<MTLCommandBuffer> commandBuffer = [_queue commandBuffer];
	
	[self updateDuDvIn:commandBuffer];
	if (_touchBegan) {
		[self drawDuDvIn:commandBuffer];
	}
	[self renderButtonIn:commandBuffer];
	
	[commandBuffer commit];
}



- (void)updateDuDvIn:(id<MTLCommandBuffer>)commandBuffer {
	MTLRenderPassDescriptor *renderPassDescriptor =
	[MTLRenderPassDescriptor renderPassDescriptor];
	renderPassDescriptor.colorAttachments[0].texture = _dudvMap;
	renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionLoad;
	renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
	id<MTLRenderCommandEncoder> encoder =
	[commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
	[encoder setRenderPipelineState:_pipelineDuDvUpdate];
	[encoder setFragmentTexture:_dudvMap atIndex:0];
	[encoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4];
	[encoder endEncoding];
}



- (void)drawDuDvIn:(id<MTLCommandBuffer>)commandBuffer {
	MTLRenderPassDescriptor *renderPassDescriptor =
	[MTLRenderPassDescriptor renderPassDescriptor];
	renderPassDescriptor.colorAttachments[0].texture = _dudvMap;
	renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionLoad;
	renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
	
	simd_float2 invSize = 1.0 / simd_make_float2(_dudvMap.width, _dudvMap.height);
	float R = _maxRadius * 8;
	simd_float2 invSizeR = invSize * R;
	
	id<MTLRenderCommandEncoder> encoder =
	[commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
	[encoder setRenderPipelineState:_pipelineDuDvDraw];
	[encoder setVertexBytes:&_touchPosition length:sizeof(simd_float2) atIndex:0];
	[encoder setVertexBytes:&invSize length:sizeof(simd_float2) atIndex:1];
	[encoder setVertexBytes:&R length:sizeof(float) atIndex:2];
	[encoder setFragmentBytes:&invSizeR length:sizeof(simd_float2) atIndex:0];
	[encoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4];
	[encoder endEncoding];
}



- (void)renderButtonIn:(id<MTLCommandBuffer>)commandBuffer {
	CAMetalLayer *metalLayer = (CAMetalLayer*)self.layer;
	id<CAMetalDrawable> drawable = [metalLayer nextDrawable];
	
	MTLRenderPassDescriptor *renderPassDescriptor =
	[MTLRenderPassDescriptor renderPassDescriptor];
	renderPassDescriptor.colorAttachments[0].texture = drawable.texture;
	renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
	renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0, 0, 0, 0);
	renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
	
	simd_float2 invSize = 1.0 / simd_make_float2(drawable.texture.width, drawable.texture.height);
	if (_touchIsOut) {
		_crashTime += 1.0 / 60.0;
	}
	
	id<MTLRenderCommandEncoder> encoder =
	[commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
	[encoder setRenderPipelineState:_pipelineButton];
	[encoder setVertexTexture:_dudvMap atIndex:0];
	[encoder setVertexBuffer:_mesh offset:0 atIndex:0];
	[encoder setVertexBytes:&invSize length:sizeof(simd_float2) atIndex:1];
	[encoder setVertexBytes:&_crashTime length:sizeof(float) atIndex:2];
	[encoder setFragmentTexture:_lizardTexture atIndex:0];
	[encoder drawPrimitives:MTLPrimitiveTypeTriangle vertexStart:0 vertexCount:_meshSize];
	[encoder endEncoding];
	
	[commandBuffer presentDrawable:drawable];
}



@end
