//
//  ViewController.h
//  MetalUIDemo
//
//  Created by George Ostrobrod on 4/11/21.
//

#import <UIKit/UIKit.h>

#import "FXButton.h"



@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *image;

- (IBAction)onTouchUpInside:(UIButton *)sender;

@end

