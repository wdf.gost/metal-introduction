//
//  FXButton.metal
//  MetalUIDemo
//
//  Created by George Ostrobrod on 4/11/21.
//

#include <metal_stdlib>
using namespace metal;


typedef struct {
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;



// MARK: -


float4 mod289(float4 x)
{
	return x - floor(x * (1.0 / 289.0)) * 289.0;
}

float4 permute(float4 x)
{
	return mod289(((x*34.0)+1.0)*x);
}

float4 taylorInvSqrt(float4 r)
{
	return 1.79284291400159 - 0.85373472095314 * r;
}

float2 fade(float2 t) {
	return t*t*t*(t*(t*6.0-15.0)+10.0);
}


// Classic Perlin noise
float cnoise(float2 P)
{
	float4 Pi = floor(P.xyxy) + float4(0.0, 0.0, 1.0, 1.0);
	float4 Pf = fract(P.xyxy) - float4(0.0, 0.0, 1.0, 1.0);
	Pi = mod289(Pi); // To avoid truncation effects in permutation
	float4 ix = Pi.xzxz;
	float4 iy = Pi.yyww;
	float4 fx = Pf.xzxz;
	float4 fy = Pf.yyww;
	
	float4 i = permute(permute(ix) + iy);
	
	float4 gx = fract(i * (1.0 / 41.0)) * 2.0 - 1.0 ;
	float4 gy = abs(gx) - 0.5 ;
	float4 tx = floor(gx + 0.5);
	gx = gx - tx;
	
	float2 g00 = float2(gx.x,gy.x);
	float2 g10 = float2(gx.y,gy.y);
	float2 g01 = float2(gx.z,gy.z);
	float2 g11 = float2(gx.w,gy.w);
	
	float4 norm = taylorInvSqrt(float4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
	g00 *= norm.x;
	g01 *= norm.y;
	g10 *= norm.z;
	g11 *= norm.w;
	
	float n00 = dot(g00, float2(fx.x, fy.x));
	float n10 = dot(g10, float2(fx.y, fy.y));
	float n01 = dot(g01, float2(fx.z, fy.z));
	float n11 = dot(g11, float2(fx.w, fy.w));
	
	float2 fade_xy = fade(Pf.xy);
	float2 n_x = mix(float2(n00, n01), float2(n10, n11), fade_xy.x);
	float n_xy = mix(n_x.x, n_x.y, fade_xy.y);
	return 2.3 * n_xy;
}


float perlin_noise(float2 x) {
	return cnoise(x * 2.0) * 0.5 + 0.5;
}



// MARK: -

static float3 rgb2hsv(float3 c) {
	constexpr float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	float4 p = select(float4(c.bg, K.wz), float4(c.gb, K.xy), c.b < c.g);
	float4 q = select(float4(p.xyw, c.r), float4(c.r, p.yzx), p.x < c.r);
	
	float d = q.x - min(q.w, q.y);
	float e = 6.0e-8h;
	return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}



static float3 hsv2rgb(float3 c) {
	constexpr float3 K = float3(1.0, 2.0 / 3.0, 1.0 / 3.0);
	float3 p = abs(fract(c.x + K) * 6.0 - 3.0);
	return c.z * fma(clamp(p - 1.0, 0.0, 1.0), c.y, 1.0 - c.y);
}



static float3 time_color(float2 uv, float time) {
	float2 uvl = uv * float2(1.0, 1.0);
	float2 tcs = time + float2(M_PI_2_H, 0.0);
	float2 tsc = time + float2(0.0, M_PI_2_H);
	
	float2 a = uvl + tcs;
	float2 a_cs = sin(a);
	float2 b = fma(uvl, a_cs, tsc);
	float2 b_sc = sin(b);
	
	return fma(abs(float3(b_sc, 0.4 / 0.9)), 0.9, 0.1);
}


// MARK: -


vertex ColorInOut vshButtonSimple(unsigned int vid [[ vertex_id ]]) {
	constexpr float2 verts[] = {
		float2(0, 0),
		float2(0, 1),
		float2(1, 0),
		float2(1, 1)
	};
	
	float2 cur_point = verts[vid];
	float2 pos = cur_point * 2 - 1;
	pos.y = -pos.y;
	
	ColorInOut out;
	
	out.position = float4(pos, 0, 1);
	out.texCoord = cur_point;
	
	return out;
}



fragment float4 fshButtonIdle(ColorInOut in [[stage_in]],
							  constant float &time [[buffer(0)]])
{
	float2 shift = in.texCoord - 0.5;
	float angle = atan2(shift.y, shift.x);
	
	float3 hsv = float3(0, 0.1, 1);
	float3 col = time_color(in.texCoord, time);
	hsv.x = fract(fma(angle, 0.5 / M_PI_F, 0.5) - fma(time, 0.25, -rgb2hsv(col).x));
	
	float k = smoothstep(0.0, 1.0, length(shift));
	hsv.y = fma(hsv.y, 1.0 - k, k);
	float4 res = float4(hsv2rgb(max(hsv, 0.0)), 1.0);
	
	return res;
}



fragment float4 fshButtonDisappear(ColorInOut in [[stage_in]],
								   constant float &time [[buffer(0)]],
								   constant float &specialTime [[buffer(1)]])
{
	float2 shift = in.texCoord - 0.5;
	float angle = atan2(shift.y, shift.x);
	
	float3 hsv = float3(0, 0.1, 1);
	float3 col = time_color(in.texCoord, time);
	hsv.x = fract(fma(angle, 0.5 / M_PI_F, 0.5) - fma(time, 0.25, -rgb2hsv(col).x));
	
	float k = smoothstep(0.0, 1.0, length(shift));
	hsv.y = fma(hsv.y, 1.0 - k, k);
	float4 res = float4(hsv2rgb(max(hsv, 0.0)), 1.0);
	
	float f = smoothstep(1.0 - sqrt(specialTime), 1, perlin_noise(shift) * 0.5 + 0.5);
	
	float t = smoothstep(0.45, 0.55, f);
	float t1 = 1.0 - t;
	res = (t1 * t1 * float4(0.0, 0.0, 0.0, 1.0) +
		   2 * t1 * t * float4(1.0, 0.5, 0.0, 1.0) +
		   t * t * res);
	return res * smoothstep(0.45, 0.46, f);
}
