//
//  MetalTools.m
//  MetalUIDemo
//
//  Created by George Ostrobrod on 4/11/21.
//

#import <simd/simd.h>

#import "MetalTools.h"



static float randIn(float a, float b) {
	float r = (rand() % 1001) / 1000.0;
	return r * (b - a) + a;
}



id<MTLRenderPipelineState> buildRenderPipeline(id<MTLDevice> device,
											   MTLPixelFormat format,
											   NSUInteger sampleCount,
											   NSString *fragment,
											   NSString *vertex)
{
	id<MTLLibrary> library = [device newDefaultLibrary];
	id<MTLFunction> vertexFunction = [library newFunctionWithName:vertex];
	id<MTLFunction> fragmentFunction = [library newFunctionWithName:fragment];
	
	MTLRenderPipelineDescriptor *descriptor = [MTLRenderPipelineDescriptor new];
	descriptor.sampleCount = sampleCount;
	descriptor.vertexFunction = vertexFunction;
	descriptor.fragmentFunction = fragmentFunction;
	
	descriptor.colorAttachments[0].pixelFormat = format;
	descriptor.colorAttachments[0].blendingEnabled = NO;
	
	return [device newRenderPipelineStateWithDescriptor:descriptor error:nil];
}



id<MTLTexture> createTexture(id<MTLDevice> device,
							 int width,
							 int height,
							 MTLPixelFormat format)
{
	MTLTextureDescriptor *descriptor = [MTLTextureDescriptor new];
	
	descriptor.pixelFormat = format;
	descriptor.width = width;
	descriptor.height = height;
	descriptor.storageMode = MTLStorageModePrivate;
	descriptor.usage = (MTLTextureUsageShaderRead |
						MTLTextureUsageShaderWrite |
						MTLTextureUsageRenderTarget);
	
	return [device newTextureWithDescriptor:descriptor];
}



id<MTLTexture> loadTexture(id<MTLDevice> device, UIImage *image) {
	MTKTextureLoader *loader = [[MTKTextureLoader alloc] initWithDevice:device];
	return [loader newTextureWithCGImage:image.CGImage
								 options:@{
		MTKTextureLoaderOptionTextureUsage:
			@(MTLTextureUsageShaderRead |
			MTLTextureUsageShaderWrite |
			MTLTextureUsageRenderTarget),
		MTKTextureLoaderOptionTextureStorageMode:
			@(MTLStorageModeShared),
		MTKTextureLoaderOptionSRGB:
			@(NO)
	}
								   error:nil];
}



id<MTLBuffer> buildMesh(id<MTLDevice> device,
						MTLSize size,
						MTLSize shift_,
						int cellSize,
						NSUInteger* vertexCount)
{
	NSUInteger W = size.width / cellSize;
	NSUInteger H = size.height / cellSize;
	*vertexCount = W * H * 2 * 3;
	
	NSUInteger bufSize = sizeof(simd_float4) * (*vertexCount);
	simd_float4 *vertices = malloc(bufSize);
	simd_float2 shift = simd_make_float2(shift_.width, shift_.height);
	
	for (int i = 0; i < H; ++i) {
		for (int j = 0; j < W; ++j) {
			NSUInteger offset = (i * W + j) * 6;
			vertices[offset + 0].xy = shift + simd_make_float2(j * cellSize, i * cellSize);
			vertices[offset + 1].xy = shift + simd_make_float2(j * cellSize, (i + 1) * cellSize);
			vertices[offset + 2].xy = shift + simd_make_float2((j + 1) * cellSize, (i + 1) * cellSize);
			vertices[offset + 3].xy = shift + simd_make_float2(j * cellSize, i * cellSize);
			vertices[offset + 4].xy = shift + simd_make_float2((j + 1) * cellSize, (i + 1) * cellSize);
			vertices[offset + 5].xy = shift + simd_make_float2((j + 1) * cellSize, i * cellSize);
			
			float a = randIn(0, 1);
			float b = randIn(a, 1);
			vertices[offset + 0].zw =
			vertices[offset + 1].zw =
			vertices[offset + 2].zw =
			vertices[offset + 3].zw =
			vertices[offset + 4].zw =
			vertices[offset + 5].zw = simd_make_float2(a, b);
		}
	}
	
	id<MTLBuffer> buffer = [device newBufferWithBytes:vertices length:bufSize options:MTLResourceStorageModeShared];
	free(vertices);
	
	return buffer;
}
