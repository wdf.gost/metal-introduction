//
//  FXButton.m
//  MetalUIDemo
//
//  Created by George Ostrobrod on 4/11/21.
//

#import <Metal/Metal.h>

#import "FXButton.h"
#import "MetalTools.h"


typedef NS_ENUM(NSUInteger, FXButtonState) {
	FXButtonStateIdle,
	FXButtonStateAnimated,
	FXButtonStateHidden,
};



@implementation FXButton
{
	id<MTLDevice> _device;
	id<MTLCommandQueue> _queue;
	
	id<MTLRenderPipelineState> _pipelineIdle;
	id<MTLRenderPipelineState> _pipelineDisappear;
	
	CADisplayLink *_displayLink;
	
	FXButtonState _animationState;
	float _timestamp;
	float _specialTimer;
}



+ (Class)layerClass {
	return CAMetalLayer.class;
}



- (void)setAnimationState:(FXButtonState)animationState {
	_animationState = animationState;
	_specialTimer = 0;
	if (animationState == FXButtonStateAnimated) {
		_specialTimer = 1;
	}
}


- (FXButtonState)animationState {
	return _animationState;
}



- (void)didMoveToWindow {
	[super didMoveToWindow];
	
	if(self.window == nil) {
		// If moving off of a window destroy the display link.
		[_displayLink invalidate];
		_displayLink = nil;
		return;
	}
	
	[_displayLink invalidate];
	_displayLink = [self.window.screen displayLinkWithTarget:self selector:@selector(render)];
	_displayLink.paused = NO;
	_displayLink.preferredFramesPerSecond = 60;
	[_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}



- (instancetype)initWithCoder:(NSCoder *)coder {
	self = [super initWithCoder:coder];
	
	_device = ((CAMetalLayer *)self.layer).device;
	_queue = [_device newCommandQueue];
	
	CAMetalLayer *metalLayer = (CAMetalLayer*)self.layer;
	id<CAMetalDrawable> drawable = [metalLayer nextDrawable];
	_pipelineIdle = buildRenderPipeline(_device,
										drawable.texture.pixelFormat,
										drawable.texture.sampleCount,
										@"fshButtonIdle",
										@"vshButtonSimple");
	_pipelineDisappear = buildRenderPipeline(_device,
											 drawable.texture.pixelFormat,
											 drawable.texture.sampleCount,
											 @"fshButtonDisappear",
											 @"vshButtonSimple");
	
	
	
	int W = (int)drawable.texture.width;
	int H = (int)drawable.texture.height;
	CAShapeLayer *maskLayer = [CAShapeLayer layer];
	maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, W, H)
												cornerRadius:16].CGPath;
	metalLayer.mask = maskLayer;
	
	return self;
}



- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	self.animationState = FXButtonStateAnimated;
	
	[super touchesBegan:touches withEvent:event];
}


- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	
	
	[super touchesMoved:touches withEvent:event];
}



- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	self.animationState = FXButtonStateIdle;
	
	[super touchesEnded:touches withEvent:event];
}



- (void)render {
	CAMetalLayer *metalLayer = (CAMetalLayer*)self.layer;
	id<CAMetalDrawable> drawable = [metalLayer nextDrawable];
	
	id<MTLCommandBuffer> commandBuffer = [_queue commandBuffer];
	{
		MTLRenderPassDescriptor *renderPassDescriptor =
		[MTLRenderPassDescriptor renderPassDescriptor];
		renderPassDescriptor.colorAttachments[0].texture = drawable.texture;
		renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
		renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0, 0, 0, 0);
		renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
		
		id<MTLRenderCommandEncoder> encoder =
		[commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
		
		switch(_animationState) {
			case FXButtonStateIdle:
				[encoder setRenderPipelineState:_pipelineIdle];
				[encoder setFragmentBytes:&_timestamp length:sizeof(float) atIndex:0];
				[encoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4];
				break;
			case FXButtonStateAnimated:
				[encoder setRenderPipelineState:_pipelineDisappear];
				[encoder setFragmentBytes:&_timestamp length:sizeof(float) atIndex:0];
				[encoder setFragmentBytes:&_specialTimer length:sizeof(float) atIndex:1];
				[encoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4];
				_specialTimer -= 1.0 / 60.0;
				if (_specialTimer <= 0) {
					_animationState = FXButtonStateHidden;
				}
				break;
			case FXButtonStateHidden:
				// TODO: stop animation.
				break;
		}
		_timestamp += 1.0 / 120.0;
		[encoder endEncoding];
	}
	[commandBuffer presentDrawable:drawable];
	[commandBuffer commit];
}



@end
