//
//  RenderView.swift
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 12/2/21.
//

import Foundation
import MetalKit
import Metal



class RenderView: MTKView {
	var renderers: [RendererSimple]!
	var currentRenderer: Int = 0 {
		didSet {
			delegate = renderers[currentRenderer]
		}
	}
	
	init() {
		super.init(frame: .zero, device: MTLCreateSystemDefaultDevice())
		// Make sure we are on a device that can run metal!
		guard let _ = device else {
			fatalError("Device loading error")
		}
		colorPixelFormat = .bgra8Unorm
		preferredFramesPerSecond = 120
		
		createRenderer()
	}
	
	required init(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func createRenderer() {
		renderers = [
			RendererSimple(metalKitView: self)!,
			RendererSimpleQuad(metalKitView: self)!,
			RendererSimpleQuad_Viewport(metalKitView: self)!,
			RendererSimpleQuad_Scissor(metalKitView: self)!,
			RendererSimpleQuad_Triangles(metalKitView: self)!,
			RendererSimpleQuad_Line(metalKitView: self)!,
			RendererSimpleQuad_LineStrip(metalKitView: self)!,
			RendererSimpleQuad_Point(metalKitView: self)!,
			RendererSimpleQuad_Instances(metalKitView: self)!,
			RendererSimpleQuad_DrawCalls(metalKitView: self)!,
			RendererMesh(metalKitView: self)!,
			RendererMesh_Attributes(metalKitView: self)!,
			RendererMesh_Texture(metalKitView: self)!,
			Renderer_Compute(metalKitView: self)!,
			Renderer_OffScreen(metalKitView: self)!,
		]
		delegate = renderers[currentRenderer]
	}
}
