//
//  Renderer14OffScreen_MPS.swift
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 18/2/21.
//

import MetalKit
import Metal
import MetalPerformanceShaders



class Renderer_OffScreen: RendererSimple {
	
	var finalPipeline: MTLRenderPipelineState
	var renderPipeline: MTLRenderPipelineState
	var computePipeline: MTLComputePipelineState
	var vertices: MTLBuffer
	var instances: Int = 16384
	var outTexture: MTLTexture?
	var mpsGaussianBlur: MPSImageGaussianBlur
	
	override init?(metalKitView: MTKView) {
		do {
			finalPipeline = try buildRenderPipeline(device: metalKitView.device!,
													metalKitView: metalKitView,
													fragment: "fshMeshTexturePremultiply",
													vertex: "vertexShader")
		} catch {
			print("Unable to compile render pipeline state.  Error info: \(error)")
			return nil
		}
		do {
			renderPipeline = try buildRenderPipeline(device: metalKitView.device!,
													 metalKitView: metalKitView,
													 fragment: "fshCompute",
													 vertex: "vshCompute")
		} catch {
			print("Unable to compile render pipeline state.  Error info: \(error)")
			return nil
		}
		do {
			computePipeline = try buildComputePipeline(device: metalKitView.device!,
													   kernel: "krnCompute")
		} catch {
			print("Unable to compile render pipeline state.  Error info: \(error)")
			return nil
		}
		
		if let v = metalKitView.device!.makeBuffer(length: instances * MemoryLayout<SIMD4<Float32>>.size,
												   options: .storageModePrivate)
		{
			vertices = v
		} else {
			return nil
		}
		
		mpsGaussianBlur = MPSImageGaussianBlur(device: metalKitView.device!, sigma: 5)
		
		super.init(metalKitView: metalKitView)
	}
	
	
	override func description() -> String {
		return "OffScreen + MPS"
	}
	
	
	
	// MARK: - Render
	override func render(in commandBuffer: MTLCommandBuffer) {
		
		if outTexture == nil {
			outTexture = createTexture(device: metalView.device!,
									   width: Int(size.width),
									   height: Int(size.height),
									   format: metalView.colorPixelFormat)
		}
		
		// Compute particles
		if let computeEncoder = commandBuffer.makeComputeCommandEncoder() {
			computeEncoder.setComputePipelineState(computePipeline)
			computeEncoder.setBuffer(vertices, offset: 0, index: 0)
			
			assert(instances % 16 == 0)
			
			let threadgroupSize = MTLSizeMake(instances / 16, 1, 1);
			let threadgroupCount = MTLSizeMake((instances + threadgroupSize.width - 1) / threadgroupSize.width, 1, 1);
			computeEncoder.dispatchThreadgroups(threadgroupCount, threadsPerThreadgroup: threadgroupSize)
			computeEncoder.endEncoding()
		}
		
		// Render particles
		let renderEncoderDescr = MTLRenderPassDescriptor()
		renderEncoderDescr.colorAttachments[0].texture = outTexture
		renderEncoderDescr.colorAttachments[0].clearColor = MTLClearColorMake(0,0,0,0)
		renderEncoderDescr.colorAttachments[0].loadAction = .clear
		if let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderEncoderDescr) {
			renderEncoder.setRenderPipelineState(renderPipeline)
			renderEncoder.setVertexBuffer(vertices, offset: 0, index: 0)
			renderEncoder.drawPrimitives(type: .point, vertexStart: 0, vertexCount: instances)
			renderEncoder.endEncoding()
		}
		
		// MPS
		mpsGaussianBlur.encode(commandBuffer: commandBuffer,
							   inPlaceTexture: &outTexture!)
		
		// Final draw
		let finalRPD = self.metalView.currentRenderPassDescriptor
		if let finalRPD = finalRPD {
			finalRPD.colorAttachments[0].clearColor = MTLClearColorMake(0,0.2,0.2,1)
			finalRPD.colorAttachments[0].loadAction = .clear
			if let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: finalRPD) {
				renderEncoder.setRenderPipelineState(finalPipeline)
				renderEncoder.setFragmentTexture(outTexture, index: 0)
				renderEncoder.drawPrimitives(type: .triangleStrip, vertexStart: 0, vertexCount: 4)
				renderEncoder.endEncoding()
			}
		}
	}
	
}
