//
//  RendererMesh.swift
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 16/2/21.
//

import MetalKit
import Metal



class RendererMesh: RendererSimple {
	
	var pipeline: MTLRenderPipelineState
	var vertices: MTLBuffer?
	var indices: MTLBuffer?
	var mesh: ImageMesh
	
	override init?(metalKitView: MTKView) {
		do {
			pipeline = try buildRenderPipeline(device: metalKitView.device!,
											   metalKitView: metalKitView,
											   fragment: "fshMeshColor",
											   vertex: "vshMeshBuffers")
		} catch {
			print("Unable to compile render pipeline state.  Error info: \(error)")
			return nil
		}
		
		if let m = loadImageMesh("Cat2") {
			mesh = m
		} else {
			return nil
		}
		
		vertices = metalKitView.device!.makeBuffer(bytes: &mesh.vertices,
												   length: MemoryLayout<SIMD2<Float>>.size * mesh.vertices.count,
												   options: .storageModeShared)
		indices = metalKitView.device!.makeBuffer(bytes: &mesh.indices,
												   length: MemoryLayout<ushort>.size * mesh.indices.count,
												   options: .storageModeShared)
		
		super.init(metalKitView: metalKitView)
	}
	
	
	override func description() -> String {
		return "Mesh - Buffer"
	}
	
	
	
	// MARK: - Render
	override func render(in commandBuffer: MTLCommandBuffer) {
		let finalRPD = self.metalView.currentRenderPassDescriptor
		if let finalRPD = finalRPD {
			finalRPD.colorAttachments[0].clearColor = MTLClearColorMake(0,0.2,0.2,1)
			finalRPD.colorAttachments[0].loadAction = .clear
			if let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: finalRPD) {
				renderEncoder.setRenderPipelineState(pipeline)
				
				var color: SIMD4<Float32> = SIMD4(0, 1, 0, 1)
				renderEncoder.setFragmentBytes(&color, length: MemoryLayout<SIMD4<Float32>>.size, index: 0)
				
				renderEncoder.setVertexBuffer(vertices, offset: 0, index: 0)
				renderEncoder.setVertexBuffer(indices, offset: 0, index: 1)
				renderEncoder.setVertexBytes(&mesh.imageSize, length: MemoryLayout<SIMD2<Float32>>.size, index: 2)
				
				renderEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: mesh.indices.count)
				
				renderEncoder.endEncoding()
			}
		}
	}
	
}
