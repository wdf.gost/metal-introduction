//
//  RendererMesh.swift
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 16/2/21.
//

import MetalKit
import Metal



class RendererMesh_Texture: RendererSimple {
	
	var pipeline: MTLRenderPipelineState
	var vertices: MTLBuffer?
	var indices: MTLBuffer?
	var mesh: ImageMesh
	var image: MTLTexture?
	
	override init?(metalKitView: MTKView) {
		
		let vertexDescriptor = MTLVertexDescriptor()
		vertexDescriptor.attributes[0].format = MTLVertexFormat.float3
		vertexDescriptor.attributes[0].offset = 0
		vertexDescriptor.attributes[0].bufferIndex = 0
		
		vertexDescriptor.layouts[0].stride = MemoryLayout<SIMD2<Float32>>.size
		vertexDescriptor.layouts[0].stepRate = 1
		vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunction.perVertex
		
		do {
			pipeline = try buildRenderPipeline(device: metalKitView.device!,
											   metalKitView: metalKitView,
											   fragment: "fshMeshTexture",
											   vertex: "vshMeshTexture",
											   vertexDescriptor: vertexDescriptor)
		} catch {
			print("Unable to compile render pipeline state.  Error info: \(error)")
			return nil
		}
		
		if let m = loadImageMesh("Cat2") {
			mesh = m
		} else {
			return nil
		}
		
		image = loadTexture(device: metalKitView.device!, imageName: "Cat2")
		
		vertices = metalKitView.device!.makeBuffer(bytes: &mesh.vertices,
												   length: MemoryLayout<SIMD2<Float>>.size * mesh.vertices.count,
												   options: .storageModeShared)
		indices = metalKitView.device!.makeBuffer(bytes: &mesh.indices,
												  length: MemoryLayout<ushort>.size * mesh.indices.count,
												  options: .storageModeShared)
		
		super.init(metalKitView: metalKitView)
	}
	
	
	override func description() -> String {
		return "Mesh - Texture"
	}
	
	
	
	// MARK: - Render
	override func render(in commandBuffer: MTLCommandBuffer) {
		let finalRPD = self.metalView.currentRenderPassDescriptor
		if let finalRPD = finalRPD {
			finalRPD.colorAttachments[0].clearColor = MTLClearColorMake(0,0.2,0.2,1)
			finalRPD.colorAttachments[0].loadAction = .clear
			if let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: finalRPD) {
				renderEncoder.setRenderPipelineState(pipeline)
				
				renderEncoder.setFragmentTexture(image, index: 0)
				
				renderEncoder.setVertexBuffer(vertices, offset: 0, index: 0)
				renderEncoder.setVertexBytes(&mesh.imageSize, length: MemoryLayout<SIMD2<Float32>>.size, index: 2)
				
				renderEncoder.drawIndexedPrimitives(type: .triangle,
													indexCount: mesh.indices.count,
													indexType: .uint16,
													indexBuffer: indices!,
													indexBufferOffset: 0)
				
				renderEncoder.endEncoding()
			}
		}
	}
	
}
