//
//  ContentView.swift
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 12/2/21.
//

import SwiftUI

// MARK: - RenderViewWrapper

final class RenderViewWrapper: UIViewRepresentable {
	var view = RenderView()
	
	func makeUIView(context: Context) -> UIView {
		return view
	}
	func updateUIView(_ uiView: UIView, context: Context) {
		
	}
	func nextExample() {
		view.currentRenderer = (view.currentRenderer + 1) % view.renderers.count;
	}
	func prevExample() {
		view.currentRenderer = ((view.currentRenderer - 1) + view.renderers.count) % view.renderers.count;
	}
	func description() -> String {
		return "\(view.currentRenderer) - \(view.renderers[view.currentRenderer].description())"
	}
}



// MARK: - View

struct ContentView: View {
	@State private var exampleDescription = "0 - Basic"
	
	var mainView = RenderViewWrapper()
	
    var body: some View {
		VStack(content: {
			self.mainView
				.frame(width: UIScreen.main.bounds.width,
					   height: UIScreen.main.bounds.width,
					   alignment: .top)
			HStack(content: {
				Button("<") {
					mainView.prevExample()
					exampleDescription = mainView.description()
				}
				Text(exampleDescription)
				Button(">") {
					mainView.nextExample()
					exampleDescription = mainView.description()
				}
			})
		})
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
