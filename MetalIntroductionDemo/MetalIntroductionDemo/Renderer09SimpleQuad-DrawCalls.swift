import MetalKit
import Metal



class RendererSimpleQuad_DrawCalls: RendererSimple {
	
	var pipelineA: MTLRenderPipelineState
	var pipelineB: MTLRenderPipelineState
	
	override init?(metalKitView: MTKView) {
		do {
			pipelineA = try buildRenderPipeline(device: metalKitView.device!,
												metalKitView: metalKitView,
												fragment: "fragmentShader",
												vertex: "vshInstances")
			pipelineB = try buildRenderPipeline(device: metalKitView.device!,
												metalKitView: metalKitView,
												fragment: "fshPoint",
												vertex: "vshPoint")
		} catch {
			print("Unable to compile render pipeline state.  Error info: \(error)")
			return nil
		}
		
		super.init(metalKitView: metalKitView)
	}
	
	
	override func description() -> String {
		return "Color Quad - Draw Calls"
	}
	
	
	
	// MARK: - Render
	override func render(in commandBuffer: MTLCommandBuffer) {
		let finalRPD = self.metalView.currentRenderPassDescriptor
		if let finalRPD = finalRPD {
			finalRPD.colorAttachments[0].clearColor = MTLClearColorMake(0,0.2,0.2,1)
			finalRPD.colorAttachments[0].loadAction = .clear
			if let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: finalRPD) {
				renderEncoder.setRenderPipelineState(pipelineA)
				renderEncoder.drawPrimitives(type: .triangleStrip, vertexStart: 0, vertexCount: 4, instanceCount: 15)
				
				renderEncoder.setRenderPipelineState(pipelineB)
				renderEncoder.drawPrimitives(type: .point, vertexStart: 0, vertexCount: 4)
				
				renderEncoder.endEncoding()
			}
		}
	}
	
}
