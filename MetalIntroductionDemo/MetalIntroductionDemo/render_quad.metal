//
//  render.metal
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 13/2/21.
//

#include <metal_stdlib>
using namespace metal;


typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;



vertex ColorInOut vertexShader(unsigned int vid [[vertex_id]])
{
	ColorInOut out;
	
	constexpr float2 vertices[] = {
		float2(0, 0),
		float2(1, 0),
		float2(0, 1),
		float2(1, 1)
	};
	
	float2 position = vertices[vid % 4];
	
	out.texCoord = position;
	
	out.position = float4(position * 2.0 - 1.0, 0.0, 1.0);
	
	return out;
}



fragment float4 fragmentShader(ColorInOut in [[stage_in]])
{
	return float4(in.texCoord, 0, 1);
}
