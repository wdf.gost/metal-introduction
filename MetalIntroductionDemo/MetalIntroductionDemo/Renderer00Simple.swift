//
//  RendererSimple.swift
//  MetalIntroductionDemo
//
//  Created by George Ostrobrod on 13/2/21.
//

import Foundation
import MetalKit
import Metal



class RendererSimple: NSObject, MTKViewDelegate {
	let device: MTLDevice
	let commandQueue: MTLCommandQueue
	var metalView: MTKView
	var size: CGSize {
		get {
			return CGSize(width: metalView.frame.width * metalView.contentScaleFactor,
						  height: metalView.frame.height * metalView.contentScaleFactor)
		}
	}
	
	
	init?(metalKitView: MTKView) {
		self.metalView = metalKitView
		self.device = metalKitView.device!
		guard let queue = self.device.makeCommandQueue() else { return nil }
		self.commandQueue = queue
		
		metalKitView.depthStencilPixelFormat = MTLPixelFormat.invalid
		metalKitView.colorPixelFormat = MTLPixelFormat.bgra8Unorm
		metalKitView.sampleCount = 1
		
		super.init()
	}
	
	func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
		
	}
	
	
	func description() -> String {
		return "Basic"
	}
	
	
	
	// MARK: - Render
	
	func draw(in view: MTKView) {
		if let commandBuffer = commandQueue.makeCommandBuffer() {
			commandBuffer.label = "App Final CB"
			
			render(in: commandBuffer)
			
			if let drawable = view.currentDrawable {
				commandBuffer.present(drawable)
			}
			
			commandBuffer.commit()
		}
	}
	
	func render(in commandBuffer: MTLCommandBuffer) {
		let finalRPD = self.metalView.currentRenderPassDescriptor
		if let finalRPD = finalRPD {
			finalRPD.colorAttachments[0].clearColor = MTLClearColorMake(0,0.2,0.2,1)
			finalRPD.colorAttachments[0].loadAction = .clear
			if let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: finalRPD) {
				renderEncoder.endEncoding()
			}
		}
	}
}
