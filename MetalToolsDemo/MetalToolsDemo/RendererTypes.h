//
//  RendererTypes.h
//  MetalToolsDemo
//
//  Created by George Ostrobrod on 27/2/21.
//

#ifndef RendererTypes_h
#define RendererTypes_h

#include <simd/simd.h>



typedef struct {
	simd_float2 translate;
	simd_float2 velocity;
	float angle;
	float scale;
} PositionData;


#endif /* RendererTypes_h */
