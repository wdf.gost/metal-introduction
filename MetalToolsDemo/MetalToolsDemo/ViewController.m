//
//  ViewController.m
//  MetalToolsDemo
//
//  Created by George Ostrobrod on 3/2/21.
//

#import "ViewController.h"

#import "Renderer00.h"



@implementation ViewController
{
	id<Renderer> _curRenderer;
}



- (void)viewDidLoad {
	[super viewDidLoad];
	
	_mtkView.device = MTLCreateSystemDefaultDevice();
	
	
	_curRenderer = [Renderer00 rendererWithMetalKitView:_mtkView];
	_mtkView.delegate = _curRenderer;
}

@end
