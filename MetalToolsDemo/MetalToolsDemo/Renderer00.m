//
//  Renderer00.m
//  MetalToolsDemo
//
//  Created by George Ostrobrod on 25/2/21.
//

#import <Accelerate/Accelerate.h>

#import "Renderer00.h"
#import "MetalUtils.h"



static const NSUInteger MaxInstances = 256;



@implementation Renderer00
{
	id<MTLDevice> _device;
	id<MTLCommandQueue> _commandQueue;
	MTKView *_mtkView;
	
	id<MTLRenderPipelineState> _pipelineRender;
	id<MTLComputePipelineState> _pipelineCompute;
	id<MTLTexture> _texture;
	id<MTLBuffer> _bufferVertices;
	id<MTLBuffer> _bufferIndices;
	id<MTLBuffer> _bufferTransforms;
	id<MTLBuffer> _bufferPositions;
	
	NSUInteger _instances;
	
	ImageMesh _mesh;
	float _time;
}



+ (nonnull instancetype)rendererWithMetalKitView:(nonnull MTKView *)view
{
	return [[self.class alloc] initWithMetalKitView:view];
}



-(nonnull instancetype)initWithMetalKitView:(nonnull MTKView *)view
{
	self = [super init];
	if (self) {
		_mtkView = view;
		_device = view.device;
		
		_commandQueue = [_device newCommandQueue];
		
		_instances = MaxInstances;
		
		MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor new];
		vertexDescriptor.attributes[0].format = MTLVertexFormatFloat4;
		vertexDescriptor.attributes[0].offset = 0;
		vertexDescriptor.attributes[0].bufferIndex = 0;
		vertexDescriptor.attributes[1].format = MTLVertexFormatFloat2;
		vertexDescriptor.attributes[1].offset = offsetof(VertexData, texture);
		vertexDescriptor.attributes[1].bufferIndex = 0;
		vertexDescriptor.layouts[0].stride = sizeof(VertexData);
		vertexDescriptor.layouts[0].stepRate = 1;
		vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
		_pipelineRender = buildRenderPipeline(_device,
											  _mtkView,
											  @"fshRenderer00",
											  @"vshRenderer00",
											  vertexDescriptor,
											  @"Pipeline: render");
		
		_pipelineCompute = buildComputePipeline(_device,
												@"krnRenderer00",
												@"Pipeline: compute");
		
		_mesh = loadImageMesh(@"Cat1");
		_texture = loadTexture(_device, @"Cat1pm");
		
		_bufferVertices = [_device newBufferWithBytes:_mesh.vertices
											   length:sizeof(VertexData) * _mesh.vertices_count
											  options:MTLResourceStorageModeShared];
		_bufferIndices = [_device newBufferWithBytes:_mesh.indices
											  length:sizeof(UInt16) * _mesh.indices_count
											 options:MTLResourceStorageModeShared];
		_bufferTransforms = [_device newBufferWithLength:_instances * sizeof(simd_float4x4)
												 options:MTLResourceStorageModePrivate];
		
		PositionData positions[MaxInstances];
		[self fillPositions:positions count:MaxInstances];
		_bufferPositions = [_device newBufferWithBytes:positions
												length:sizeof(PositionData) * _instances
											   options:MTLResourceStorageModeShared];
	}
	
	return self;
}



- (void)dealloc {
	free(_mesh.indices);
	free(_mesh.vertices);
}



- (MTKView *)mtkView {
	return _mtkView;
}



// MARK: - Metal



- (void)fillPositions:(PositionData *)positions count:(NSUInteger)count {
	for (NSUInteger i = 0; i < count; ++i) {
		positions[i] = (PositionData) {
			.translate = simd_make_float2(rand11(), rand11()),
			.velocity = 0.005 * simd_make_float2(rand11(), rand11()),
			.angle = rand01(),
			.scale = 0.1 + 0.8 * rand11(),
		};
	}
}



- (id<MTLTexture>)createTextureWithSize:(CGSize)size format:(MTLPixelFormat)format {
	MTLTextureDescriptor *textureDescriptor =
	[MTLTextureDescriptor texture2DDescriptorWithPixelFormat:format
													   width:size.width
													  height:size.height
												   mipmapped:NO];
	textureDescriptor.storageMode = MTLStorageModePrivate;
	textureDescriptor.usage = (MTLTextureUsageShaderRead |
							   MTLTextureUsageShaderWrite |
							   MTLTextureUsageRenderTarget);
	
	id<MTLTexture> res = [_device newTextureWithDescriptor:textureDescriptor];
	NSAssert(res != nil, @"Problems with creating new texture.");
	return res;
}



- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size
{
}



- (void)drawInMTKView:(nonnull MTKView *)view
{
	id <MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
	commandBuffer.label = @"Main Loop";
	
	_time += 1.0 / 60.0;
	
	[self updateTransformsIn:commandBuffer];
	[self renderIn:commandBuffer];
	
	[commandBuffer presentDrawable:view.currentDrawable];
	[commandBuffer commit];
}



- (void)updateTransformsIn:(id<MTLCommandBuffer>)commandBuffer {
	id<MTLComputeCommandEncoder> encoder = [commandBuffer computeCommandEncoder];
	encoder.label = @"Transform cats";
	[encoder setComputePipelineState:_pipelineCompute];
	
	[encoder setBuffer:_bufferPositions offset:0 atIndex:0];
	[encoder setBuffer:_bufferTransforms offset:0 atIndex:1];
	
	MTLSize threadgroupSize = MTLSizeMake(MAX(1, _instances / 16),
										  1,
										  1);
	MTLSize threadgroupCount = MTLSizeMake(MAX(1, (_instances + threadgroupSize.width - 1) / threadgroupSize.width), 1, 1);
	[encoder dispatchThreadgroups:threadgroupCount threadsPerThreadgroup:threadgroupSize];
	
	[encoder endEncoding];
}



- (void)renderIn:(id<MTLCommandBuffer>)commandBuffer {
	MTLRenderPassDescriptor* renderPassDescriptor = self.mtkView.currentRenderPassDescriptor;
	renderPassDescriptor.colorAttachments[0].clearColor = (MTLClearColor){0, 0.2, 0.2, 1};
	renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
	
	float ratio = _mtkView.frame.size.width / _mtkView.frame.size.height;
	
	if (renderPassDescriptor != nil) {
		id <MTLRenderCommandEncoder> encoder =
		[commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
		encoder.label = @"Draw cats";
		
		[encoder setRenderPipelineState:_pipelineRender];
		[encoder setVertexBuffer:_bufferVertices offset:0 atIndex:0];
		[encoder setVertexBuffer:_bufferTransforms offset:0 atIndex:1];
		[encoder setVertexBytes:&ratio length:sizeof(float) atIndex:2];
		[encoder setFragmentBytes:&_time length:sizeof(float) atIndex:0];
		[encoder setFragmentTexture:_texture atIndex:0];
		[encoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
							indexCount:_mesh.indices_count
							 indexType:MTLIndexTypeUInt16
						   indexBuffer:_bufferIndices
					 indexBufferOffset:0
						 instanceCount:_instances];
		
		[encoder endEncoding];
	}
}



// MARK: - Capture

+ (void)startCaptureIn:(id)target {
	MTLCaptureManager *captureManager = MTLCaptureManager.sharedCaptureManager;
	MTLCaptureDescriptor *captureDescriptor = [MTLCaptureDescriptor new];
	captureDescriptor.captureObject = target;
	[captureManager startCaptureWithDescriptor:captureDescriptor error:nil];
}



+ (void)endCapture {
	MTLCaptureManager *captureManager = MTLCaptureManager.sharedCaptureManager;
	[captureManager stopCapture];
}

@end
