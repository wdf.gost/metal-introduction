//
//  ViewController.m
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 3/2/21.
//

#import "ViewController.h"

#import "Renderer00.h"
#import "Renderer01.h"
#import "Renderer02.h"
#import "Renderer03.h"
#import "Renderer04.h"
#import "Renderer05.h"
#import "Renderer06.h"
#import "Renderer07.h"

#import "Shader00.h"
#import "Shader01.h"
#import "Shader02.h"
#import "Shader03.h"
#import "Shader04.h"
#import "Shader05.h"

#import "Texture00A.h"
#import "Texture00B.h"
#import "Texture00C.h"
#import "Texture00D.h"
#import "Texture00E.h"
#import "Texture00F.h"
#import "Texture00G.h"

#import "Texture01A.h"
#import "Texture01B.h"
#import "Texture01C.h"

#import "Compute00.h"
#import "Compute01.h"
#import "Compute00-01.h"
#import "Compute00-02.h"
#import "Compute00-04.h"
#import "Compute00-08.h"
#import "Compute00-16.h"
#import "Compute00-32.h"



typedef struct {
	/* 00 */ simd_float4 rect; // 16
	/* 16 */ bool flag;        // 1
	                           // +1
	/* 18 */ short index;      // 2
							   // +12
	/* 32 */ simd_float3 size; // 16
	/* 48 */ float f;          // 4
	                           // +12
} StructureBadPadding;



typedef struct {
	/* 00 */ simd_float4 rect; // 16
	/* 16 */ simd_float3 size; // 16
	/* 32 */ float f;          // 4
	/* 36 */ short index;      // 2
	/* 38 */ bool flag;        // 1
	                           // +9
} StructureGoodPadding;




@implementation ViewController
{
	NSArray<Class> *_renderers;
	id<Renderer> _curRenderer;
	int _curCase;
}


float foo(float a, float b) {
	return 10.0f * a * a + 5.0f * b * b;
}



- (void)viewDidLoad {
	[super viewDidLoad];
	
	NSLog(@"StructureBadPadding: %ld", sizeof(StructureBadPadding));
	NSLog(@"\trect\t: %ld\t(%ld)", offsetof(StructureBadPadding, rect), sizeof(simd_float4));
	NSLog(@"\tflag\t: %ld\t(%ld)", offsetof(StructureBadPadding, flag), sizeof(bool));
	NSLog(@"\tindex\t: %ld\t(%ld)", offsetof(StructureBadPadding, index), sizeof(short));
	NSLog(@"\tsize\t: %ld\t(%ld)", offsetof(StructureBadPadding, size), sizeof(simd_float3));
	NSLog(@"\tf\t\t: %ld\t(%ld)", offsetof(StructureBadPadding, f), sizeof(float));
	NSLog(@"StructureGoodPadding: %ld", sizeof(StructureGoodPadding));
	NSLog(@"\trect: %ld\t(%ld)", offsetof(StructureGoodPadding, rect), sizeof(simd_float4));
	NSLog(@"\trect: %ld\t(%ld)", offsetof(StructureGoodPadding, size), sizeof(simd_float3));
	NSLog(@"\trect: %ld\t(%ld)", offsetof(StructureGoodPadding, f), sizeof(float));
	NSLog(@"\trect: %ld\t(%ld)", offsetof(StructureGoodPadding, index), sizeof(short));
	NSLog(@"\trect: %ld\t(%ld)", offsetof(StructureGoodPadding, flag), sizeof(bool));
	
	
	
	_mtkView.device = MTLCreateSystemDefaultDevice();
	
	
	_renderers = @[
		[Renderer07 class],
		[Renderer06 class],
		[Renderer05 class],
		[Renderer04 class],
		[Renderer03 class],
		[Renderer02 class],
		[Renderer01 class],
		[Renderer00 class],
		
		[Shader00 class],
		[Shader01 class],
		[Shader02 class],
		[Shader03 class],
		[Shader04 class],
		[Shader05 class],
		
		[Texture00A class],
		[Texture00B class],
		[Texture00C class],
		[Texture00D class],
		[Texture00E class],
		[Texture00F class],
		[Texture00G class],
		
		[Texture01A class],
		[Texture01B class],
		[Texture01C class],
		
		[Compute00 class],
		[Compute01 class],
		[Compute00_01 class],
		[Compute00_02 class],
		[Compute00_04 class],
		[Compute00_08 class],
		[Compute00_16 class],
		[Compute00_32 class],
	];
	_curCase = 0;
	
	[self updateRenderer];
}



- (void)updateRenderer {
	FPSUpdater updater = ^(double fps) {
		self.estimatedFPS.text = [NSString stringWithFormat:@"%d", (int)fps];
	};
	_curRenderer = [((id<Renderer>)_renderers[_curCase]) rendererWithMetalKitView:_mtkView fpsUpdater:updater];
	_mtkView.delegate = _curRenderer;
	_demoName.text = _curRenderer.demoDescription;
}



- (IBAction)onNextDemo:(id)sender {
	_curCase = (_curCase + 1) % _renderers.count;
	[self updateRenderer];
}



- (IBAction)onPrevDemo:(id)sender {
	_curCase = (_curCase - 1 + (int)_renderers.count) % _renderers.count;
	[self updateRenderer];
}
@end
