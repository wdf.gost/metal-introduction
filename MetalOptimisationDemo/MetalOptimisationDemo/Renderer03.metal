//
//  Renderer00.metal
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;



typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;



typedef struct {
	float4 position [[attribute(0)]];
	float2 texture [[attribute(1)]];
} VertexData;



vertex ColorInOut vshRenderer03(VertexData in [[stage_in]],
								unsigned int iid [[instance_id]],
								device const float4x4 *transforms [[buffer(1)]])
{
	ColorInOut out;
	out.position = transforms[iid] * in.position;
	out.texCoord = in.texture;
	
	return out;
}



fragment float4 fshRenderer03(ColorInOut in [[stage_in]],
							  float4 back [[ color(0) ]],
							  texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::clamp_to_zero, filter::linear);
	
	float4 res = image.sample(imageSampler, in.texCoord);
	
	// NOTE: Source image is unpremultiplied
	res.rgb = mix(back.rgb, res.rgb, res.a);
	
	return res;
}
