//
//  Renderer.h
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#import <Foundation/Foundation.h>
#import <MetalKit/MetalKit.h>
#import <Metal/Metal.h>



#ifndef Renderer_h
#define Renderer_h



typedef void (^FPSUpdater)(double);



@protocol Renderer <MTKViewDelegate>

@property (nonatomic, readonly) NSString * _Nullable demoDescription;
@property (nonatomic, readonly) MTKView * _Nonnull mtkView;

+ (nonnull instancetype)rendererWithMetalKitView:(nonnull MTKView *)view
									  fpsUpdater:(FPSUpdater _Nonnull )fpsUpdater;
- (void)renderIn:(id<MTLCommandBuffer>_Nonnull)commandBuffer;

@end



#endif /* Renderer_h */
