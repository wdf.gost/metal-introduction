//
//  Renderer00.m
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 25/2/21.
//

#import "Texture00B.h"
#import "MetalUtils.h"



@implementation Texture00B
{
	id<MTLDevice> _device;
	id<MTLCommandQueue> _commandQueue;
	MTKView *_mtkView;
	
	id<MTLRenderPipelineState> _pipelineRender;
	id<MTLRenderPipelineState> _pipelineFinal;
	id<MTLTexture> _texture;
	id<MTLTexture> _textureOut;
	
	CFTimeInterval _prevTime;
	FPSUpdater _fpsUpdater;
	double _prevFPS[MaxFPSHistory];
	NSUInteger _prevFPSNum;
}



+ (nonnull instancetype)rendererWithMetalKitView:(nonnull MTKView *)view
									  fpsUpdater:(FPSUpdater _Nonnull )fpsUpdater
{
	return [[self.class alloc] initWithMetalKitView:view fpsUpdater:fpsUpdater];
}



-(nonnull instancetype)initWithMetalKitView:(nonnull MTKView *)view  fpsUpdater:(FPSUpdater _Nonnull )fpsUpdater
{
	self = [super init];
	if (self) {
		_fpsUpdater = fpsUpdater;
		_prevFPSNum = 0;
		
		_mtkView = view;
		_device = view.device;
		
		_commandQueue = [_device newCommandQueue];
		
		_pipelineRender = buildRenderPipeline(_device, _mtkView, @"fshTexture00B", @"vshTexture00A", nil);
		_pipelineFinal = buildRenderPipeline(_device, _mtkView, @"fshTexture00AFinal", @"vshTexture00A", nil);
		
		_texture = loadTexture(_device, @"Cat3");
		_textureOut = createTexture(_device, 1024 * 8, 1024 * 8, view.colorPixelFormat);
	}
	
	return self;
}



- (NSString *)demoDescription {
	return @"t00B - Texture cache (1)";
}


- (MTKView *)mtkView {
	return _mtkView;
}



// MARK: - Metal



- (void)fillPositions:(PositionData *)positions count:(NSUInteger)count {
	for (NSUInteger i = 0; i < count; ++i) {
		positions[i] = (PositionData) {
			.translate = simd_make_float2(rand11(), rand11()),
			.velocity = 0.005 * simd_make_float2(rand11(), rand11()),
			.angle = rand01(),
			.scale = 0.1 + 0.8 * rand11(),
		};
	}
}



- (id<MTLTexture>)createTextureWithSize:(CGSize)size format:(MTLPixelFormat)format {
	MTLTextureDescriptor *textureDescriptor =
	[MTLTextureDescriptor texture2DDescriptorWithPixelFormat:format
													   width:size.width
													  height:size.height
												   mipmapped:NO];
	textureDescriptor.storageMode = MTLStorageModePrivate;
	textureDescriptor.usage = (MTLTextureUsageShaderRead |
							   MTLTextureUsageShaderWrite |
							   MTLTextureUsageRenderTarget);
	
	id<MTLTexture> res = [_device newTextureWithDescriptor:textureDescriptor];
	NSAssert(res != nil, @"Problems with creating new texture.");
	return res;
}



- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size
{
}



- (void)drawInMTKView:(nonnull MTKView *)view
{
	id <MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
	commandBuffer.label = @"Final draw";
	
	[self renderIn:commandBuffer];
	
	[commandBuffer presentDrawable:view.currentDrawable];
	[commandBuffer commit];
	
	[self updateFPS];
}



- (void)updateFPS {
	static dispatch_once_t once;
	
	dispatch_once(&once, ^{
		_prevTime = CACurrentMediaTime() - 1.0 / 60.0;
	});
	
	CFTimeInterval curTime = CACurrentMediaTime();
	double fps = 1.0 / (curTime - _prevTime);
	
	double fpsSum = fps;
	int fpsCount = 1;
	for (NSUInteger i = 0; i < _prevFPSNum; ++i) {
		fpsSum += _prevFPS[i];
		++fpsCount;
	}
	for (int i = (int)_prevFPSNum - 1; i > 0; --i) {
		_prevFPS[i] = _prevFPS[i - 1];
	}
	_prevFPS[0] = fps;
	_prevFPSNum = MIN(_prevFPSNum + 1, MaxFPSHistory);
	_fpsUpdater(fpsSum / fpsCount);
	_prevTime = curTime;
}



- (void)renderIn:(id<MTLCommandBuffer>)commandBuffer {
	
	MTLRenderPassDescriptor* renderPassDescriptor = [MTLRenderPassDescriptor new];
	renderPassDescriptor.colorAttachments[0].texture = _textureOut;
	renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionLoad;
	if (renderPassDescriptor != nil) {
		id <MTLRenderCommandEncoder> encoder =
		[commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
		encoder.label = @"Render cats";
		
		[encoder setRenderPipelineState:_pipelineRender];
		[encoder setFragmentTexture:_texture atIndex:0];
		[encoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4];
		
		[encoder endEncoding];
	}
	
	MTLRenderPassDescriptor* finalPassDescriptor = self.mtkView.currentRenderPassDescriptor;
	finalPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionLoad;
	if (finalPassDescriptor != nil) {
		id <MTLRenderCommandEncoder> encoder =
		[commandBuffer renderCommandEncoderWithDescriptor:finalPassDescriptor];
		encoder.label = @"Final";
		
		[encoder setRenderPipelineState:_pipelineFinal];
		[encoder setFragmentTexture:_textureOut atIndex:0];
		[encoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4];
		
		[encoder endEncoding];
	}
	
}

@end
