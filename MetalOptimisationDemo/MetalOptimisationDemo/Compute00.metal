//
//  Renderer00.metal
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;

typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;

vertex ColorInOut vshCompute00(unsigned int vid [[vertex_id]])
{
	ColorInOut out;
	
	constexpr float2 vertices[] = {
		float2(0, 0),
		float2(1, 0),
		float2(0, 1),
		float2(1, 1)
	};
	
	float2 position = vertices[vid % 4];
	out.texCoord = position;
	out.position = float4(position * 2.0 - 1.0, 0.0, 1.0);
	out.position.y = -out.position.y;
	
	return out;
}



fragment float4 fshCompute00(ColorInOut in [[stage_in]],
							 texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::clamp_to_edge, filter::linear);
	return image.sample(imageSampler, in.texCoord);
}



kernel void krnCompute00(texture2d<float, access::read> in [[ texture(0) ]],
						 texture2d<float, access::write> out [[ texture(1) ]],
						 uint2 gid [[thread_position_in_grid]])
{
	uint2 buf_size = uint2(in.get_width(), in.get_height());
	if (any(gid >= buf_size)) {
		return;
	}

	float4 src = in.read(gid);
	out.write(src * src, gid);
}



kernel void krnCompute00_t(texture2d<float, access::read> in [[ texture(0) ]],
						   texture2d<float, access::write> out [[ texture(1) ]],
						   constant float &time [[buffer(0)]],
						   uint2 gid [[thread_position_in_grid]])
{
	uint2 buf_size = uint2(in.get_width(), in.get_height());
	if (any(gid >= buf_size)) {
		return;
	}
	
	float4 src = in.read(gid);
	float k = 1.0 + sin(2.0 * time);
	out.write(src * k, gid);
}
