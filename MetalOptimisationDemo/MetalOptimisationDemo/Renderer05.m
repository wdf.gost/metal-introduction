//
//  Renderer00.m
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 25/2/21.
//

#import "Renderer05.h"
#import "MetalUtils.h"



static const NSUInteger MaxInstances = 256;



@implementation Renderer05
{
	id<MTLDevice> _device;
	id<MTLCommandQueue> _commandQueue;
	MTKView *_mtkView;
	
	id<MTLRenderPipelineState> _pipelineRender;
	id<MTLTexture> _texture;
	id<MTLBuffer> _bufferTransforms;
	
	CFTimeInterval _prevTime;
	FPSUpdater _fpsUpdater;
	double _prevFPS[MaxFPSHistory];
	NSUInteger _prevFPSNum;
	
	PositionData _positions[MaxInstances];
}



+ (nonnull instancetype)rendererWithMetalKitView:(nonnull MTKView *)view
									  fpsUpdater:(FPSUpdater _Nonnull )fpsUpdater
{
	return [[self.class alloc] initWithMetalKitView:view fpsUpdater:fpsUpdater];
}



-(nonnull instancetype)initWithMetalKitView:(nonnull MTKView *)view  fpsUpdater:(FPSUpdater _Nonnull )fpsUpdater
{
	self = [super init];
	if (self) {
		_fpsUpdater = fpsUpdater;
		_prevFPSNum = 0;
		
		_mtkView = view;
		_device = view.device;
		
		_commandQueue = [_device newCommandQueue];
		
		_pipelineRender = buildRenderPipeline(_device, _mtkView, @"fshRenderer05", @"vshRenderer05", nil);
		
		_texture = loadTexture(_device, @"Cat1");
		
		_bufferTransforms = [_device newBufferWithLength:MaxInstances * sizeof(simd_float4x4)
												 options:MTLResourceStorageModeShared];
		
		[self fillPositions:_positions count:MaxInstances];
	}
	
	return self;
}



- (NSString *)demoDescription {
	NSLog(@"\nCase 05:\n"
		  @"\tCanvas size: %dx%d\n"
		  @"\tVertices per instance: %d\n"
		  @"\tInstances: %ld",
		  (int)(self.mtkView.frame.size.width * self.mtkView.contentScaleFactor),
		  (int)(self.mtkView.frame.size.height * self.mtkView.contentScaleFactor),
		  4,
		  MaxInstances);
	return @"r05 - Pack draw calls into one encoder";
}


- (MTKView *)mtkView {
	return _mtkView;
}



// MARK: - Metal



- (void)fillPositions:(PositionData *)positions count:(NSUInteger)count {
	for (NSUInteger i = 0; i < count; ++i) {
		positions[i] = (PositionData) {
			.translate = simd_make_float2(rand11(), rand11()),
			.velocity = 0.005 * simd_make_float2(rand11(), rand11()),
			.angle = rand01(),
			.scale = 0.1 + 0.8 * rand11(),
		};
	}
}



- (id<MTLTexture>)createTextureWithSize:(CGSize)size format:(MTLPixelFormat)format {
	MTLTextureDescriptor *textureDescriptor =
	[MTLTextureDescriptor texture2DDescriptorWithPixelFormat:format
													   width:size.width
													  height:size.height
												   mipmapped:NO];
	textureDescriptor.storageMode = MTLStorageModePrivate;
	textureDescriptor.usage = (MTLTextureUsageShaderRead |
							   MTLTextureUsageShaderWrite |
							   MTLTextureUsageRenderTarget);
	
	id<MTLTexture> res = [_device newTextureWithDescriptor:textureDescriptor];
	NSAssert(res != nil, @"Problems with creating new texture.");
	return res;
}



- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size
{
}



- (void)drawInMTKView:(nonnull MTKView *)view
{
	id <MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
	commandBuffer.label = @"Final draw";
	
	[self updateTransformsIn:commandBuffer];
	[self renderIn:commandBuffer];
	
	[commandBuffer presentDrawable:view.currentDrawable];
	[commandBuffer commit];
	
	[self updateFPS];
}



- (void)updateFPS {
	static dispatch_once_t once;
	
	dispatch_once(&once, ^{
		_prevTime = CACurrentMediaTime() - 1.0 / 60.0;
	});
	
	CFTimeInterval curTime = CACurrentMediaTime();
	double fps = 1.0 / (curTime - _prevTime);
	
	double fpsSum = fps;
	int fpsCount = 1;
	for (NSUInteger i = 0; i < _prevFPSNum; ++i) {
		fpsSum += _prevFPS[i];
		++fpsCount;
	}
	for (int i = (int)_prevFPSNum - 1; i > 0; --i) {
		_prevFPS[i] = _prevFPS[i - 1];
	}
	_prevFPS[0] = fps;
	_prevFPSNum = MIN(_prevFPSNum + 1, MaxFPSHistory);
	_fpsUpdater(fpsSum / fpsCount);
	_prevTime = curTime;
}



- (void)updateTransformsIn:(id<MTLCommandBuffer>)commandBuffer {
	simd_float4x4 *transforms = _bufferTransforms.contents;
	for (int i = 0; i < MaxInstances; ++i) {
		PositionData pos = _positions[i];
		
		simd_float2 cs = simd_make_float2(cos(pos.angle), sin(pos.angle));
		float s = pos.scale + 0.05 * sin(simd_length(pos.translate));
		transforms[i] =
		(
		 simd_matrix(simd_make_float4(cs.x * s, cs.y, 0, 0),
					 simd_make_float4(-cs.y, cs.x * s, 0, 0),
					 simd_make_float4(0, 0, 1, 0),
					 simd_make_float4(pos.translate.x, pos.translate.y, 0, 1))
		 );
		
		float m = -1.0 + pos.scale;
		float M = 1.0 - pos.scale;
		if (pos.translate.x < m || pos.translate.x > M) {
			pos.translate.x = simd_clamp(pos.translate.x, m, M);
			pos.velocity.x = -pos.velocity.x;
		}
		if (pos.translate.y < m || pos.translate.y > M) {
			pos.translate.y = simd_clamp(pos.translate.y, m, M);
			pos.velocity.y = -pos.velocity.y;
		}
		pos.translate += pos.velocity;
		pos.angle += simd_length(pos.velocity) * pos.translate.x * pos.translate.y * 10.0;
		
		_positions[i] = pos;
	}
}



- (void)renderIn:(id<MTLCommandBuffer>)commandBuffer {
	MTLRenderPassDescriptor* renderPassDescriptor = self.mtkView.currentRenderPassDescriptor;
	renderPassDescriptor.colorAttachments[0].clearColor = (MTLClearColor){0, 0.2, 0.2, 1};
	renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
	
	if (renderPassDescriptor != nil) {
		id <MTLRenderCommandEncoder> encoder =
		[commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
		encoder.label = @"Render cats";
		
		[encoder setRenderPipelineState:_pipelineRender];
		[encoder setVertexBuffer:_bufferTransforms offset:0 atIndex:1];
		[encoder setFragmentTexture:_texture atIndex:0];
		
		for (int i = 0; i < MaxInstances; ++i) {
			[encoder setVertexBytes:&i length:sizeof(int) atIndex:0];
			[encoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4];
		}
		
		[encoder endEncoding];
	}
}

@end
