//
//  Renderer00.metal
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;



typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;



typedef struct {
	float4 dst [[ color(1) ]];
} FragmentOut1;



typedef struct {
	float4 dst [[ color(0) ]];
} FragmentOut2;



fragment FragmentOut1 fshTexture01B_1(ColorInOut in [[stage_in]],
									  texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::repeat, filter::nearest);
	
	float2 uv = in.texCoord;
	float4 src = image.sample(imageSampler, uv);
	return {src * src};
}



fragment FragmentOut2 fshTexture01B_2(ColorInOut in [[stage_in]],
									  const float4 image [[ color(1) ]])
{
	float4 src = image;
	return {sqrt(src)};
}
