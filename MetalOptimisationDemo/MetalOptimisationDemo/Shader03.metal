//
//  Renderer00.metal
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;



typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;



// MARK: - Code must be optimised:

static half3 rgb2hsv(half3 c) {
	half M = max3(c.x, c.y, c.z);
	half m = min3(c.x, c.y, c.z);
	half C = M - m;
	
	half h = 0.0h;
	if (C > 0.0h) {
		if (c.r == M) {
			h = fmod((c.g - c.b) / C, 6.0h);
		} else if (c.g == M) {
			h = (c.b - c.r) / C + 2.0h;
		} else {
			h = (c.r - c.g) / C + 4.0h;
		}
	}
	h /= 6.0h;
	
	half v = M;
	
	half s = 0.0h;
	if (v > 0.0h) {
		s = C / v;
	}
	
	return clamp(half3(h, s, v), 0.0h, 1.0h);
}



static half3 hsv2rgb(half3 c) {
	half h = c.x * 6.0h;
	half s = c.y;
	half v = c.z;
	
	half C = v * s;
	half X = C * (1.0h - abs(fmod(h, 2.0h) - 1.0h));
	
	half3 rgb = 0.0h;
	if (h < 0.0h) {
		return rgb;
	}
	if (h <= 1.0h) {
		rgb = half3(C, X, 0.0h);
	} else if (h <= 2.0h) {
		rgb = half3(X, C, 0.0h);
	} else if (h <= 3.0h) {
		rgb = half3(0.0h, C, X);
	} else if (h <= 4.0h) {
		rgb = half3(0.0h, X, C);
	} else if (h <= 5.0h) {
		rgb = half3(X, 0.0h, C);
	} else if (h <= 6.0h) {
		rgb = half3(C, 0.0h, X);
	}
	
	half m = v - C;
	rgb += m;
	
	return clamp(rgb, 0.0, 1.0);
}



static half4 blend(half4 top, half4 btm) {
	return fma(1.0 - top.a, btm, top);
}



static half3 time_color(half2 uv, half time) {
	return fma(abs(half3(sin(fma(cos(fma(3.0h, uv.y, time)), 2.0h * uv.x, time)),
						 cos(fma(sin(fma(2.0h, uv.x, time)), 3.0h * uv.y, time)),
						 0.4 / 0.9h)), 0.9, 0.1h);
}



fragment half4 fshShader03(ColorInOut in [[stage_in]],
						   constant float &_time [[buffer(0)]],
						   texture2d<half> image0 [[ texture(0) ]],
						   texture2d<half> image1 [[ texture(1) ]])
{
	constexpr sampler image0Sampler(address::clamp_to_zero, filter::linear);
	constexpr sampler image1Sampler(address::repeat, filter::linear);
	
	float2 uv0 = in.texCoord;
	float2 uv1 = in.texCoord;
	
	half time = half(_time);
	
	
	// Foreground effect
	
	// - Foreground shift
	float2 shift = uv0 - 0.5;
	float angle = atan2(shift.y, shift.x);
	uv0 = fma(shift, fma(0.1, cos(fma(10.0, angle, 2.0 * time)), 1.0), 0.5);
	
	half4 src = image0.sample(image0Sampler, uv0);
	
	
	// Background effect
	float2 pixel = 1.0 / float2(image1.get_width(), image1.get_height());
	
	// - Sketch
	// 012
	// 345
	// 678
	half4 dst_pixels[9] = {
		image1.sample(image1Sampler, fma(pixel, float2(-1.0, -1.0), uv1)),
		image1.sample(image1Sampler, fma(pixel, float2(00.0, -1.0), uv1)),
		image1.sample(image1Sampler, fma(pixel, float2(+1.0, -1.0), uv1)),
		
		image1.sample(image1Sampler, fma(pixel, float2(-1.0, 00.0), uv1)),
		image1.sample(image1Sampler, fma(pixel, float2(00.0, 00.0), uv1)),
		image1.sample(image1Sampler, fma(pixel, float2(+1.0, 00.0), uv1)),
		
		image1.sample(image1Sampler, fma(pixel, float2(-1.0, +1.0), uv1)),
		image1.sample(image1Sampler, fma(pixel, float2(00.0, +1.0), uv1)),
		image1.sample(image1Sampler, fma(pixel, float2(+1.0, +1.0), uv1)),
	};
	
	half4 outline = fma(8.0h,
						dst_pixels[4],
						-(dst_pixels[0] + dst_pixels[1] + dst_pixels[2] +
						  dst_pixels[3] +                 dst_pixels[5] +
						  dst_pixels[6] + dst_pixels[7] + dst_pixels[8]));
	half4 dst = dst_pixels[4] * smoothstep(0.8h, 1.0h, 1.0h - length(outline));
	
	// - HSV
	half3 hsv = rgb2hsv(dst.rgb);
	half3 col = time_color(half2(uv1), time);
	hsv.x = fract(fma(half(angle), 0.5h / M_PI_H, 0.5h) - fma(time, 0.25h, -rgb2hsv(col).x));
	
	half k = smoothstep(0.0h, 1.0h, length(half2(shift)));
	hsv.y = fma(hsv.y, 1.0h - k, k);
	dst.rgb = hsv2rgb(hsv);
	
	return blend(src, dst);
}
