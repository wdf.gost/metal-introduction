//
//  Renderer00.metal
//  MetalOptimisationDemo
//
//  Created by George Ostrobrod on 26/2/21.
//

#include <metal_stdlib>
#include "RendererTypes.h"

using namespace metal;



typedef struct
{
	float4 position [[position]];
	float2 texCoord;
} ColorInOut;



vertex ColorInOut vshShader(unsigned int vid [[vertex_id]])
{
	ColorInOut out;
	
	constexpr float2 vertices[] = {
		float2(0, 0),
		float2(1, 0),
		float2(0, 1),
		float2(1, 1)
	};
	
	float2 position = vertices[vid % 4];
	out.texCoord = position;
	out.position = float4(position * 2.0 - 1.0, 0.0, 1.0);
	out.position.y = -out.position.y;
	
	return out;
}


fragment float4 fshShaderFinal(ColorInOut in [[stage_in]],
							   texture2d<float> image [[ texture(0) ]])
{
	constexpr sampler imageSampler(address::clamp_to_edge, filter::linear);
	return image.sample(imageSampler, in.texCoord);
}



// MARK: - Code must be optimised:

static float3 rgb2hsv(float3 c) {
	float M = max(c.x, max(c.y, c.z));
	float m = min(c.x, min(c.y, c.z));
	float C = M - m;
	
	float h = 0;
	if (C > 0) {
		if (c.r == M) {
			h = fmod((c.g - c.b) / C, 6.0);
		} else if (c.g == M) {
			h = (c.b - c.r) / C + 2;
		} else {
			h = (c.r - c.g) / C + 4;
		}
	}
	h /= 6.0;
	
	float v = M;
	
	float s = 0;
	if (v > 0) {
		s = C / v;
	}
	
	return clamp(float3(h, s, v), 0.0, 1.0);
}



static float3 hsv2rgb(float3 c) {
	float h = c.x * 6.0;
	float s = c.y;
	float v = c.z;
	
	float C = v * s;
	float X = C * (1.0 - abs(fmod(h, 2.0) - 1.0));
	
	float3 rgb = 0;
	if (h < 0) {
		return rgb;
	}
	if (h <= 1) {
		rgb = float3(C, X, 0);
	} else if (h <= 2) {
		rgb = float3(X, C, 0);
	} else if (h <= 3) {
		rgb = float3(0, C, X);
	} else if (h <= 4) {
		rgb = float3(0, X, C);
	} else if (h <= 5) {
		rgb = float3(X, 0, C);
	} else if (h <= 6) {
		rgb = float3(C, 0, X);
	}
	
	float m = v - C;
	rgb += m;
	
	return clamp(rgb, 0.0, 1.0);
}



static float4 blend(float4 top, float4 btm) {
	float4 res;
	
	res.rgb = top.rgb + btm.rgb * (1.0 - top.a);
	res.a = top.a + btm.a * (1.0 - top.a);
	
	return res;
}


static float3 time_color(float2 uv, float time) {
	return float3(0.1 + 0.9 * abs(sin(cos(time + 3.0 * uv.y) * 2.0 * uv.x + time)),
				  0.1 + 0.9 * abs(cos(sin(time + 2.0 * uv.x) * 3.0 * uv.y + time)),
				  0.5);
}



fragment float4 fshShader00(ColorInOut in [[stage_in]],
							constant float &time [[buffer(0)]],
							texture2d<float> image0 [[ texture(0) ]],
							texture2d<float> image1 [[ texture(1) ]])
{
	constexpr sampler image0Sampler(address::clamp_to_zero, filter::linear);
	constexpr sampler image1Sampler(address::repeat, filter::linear);
	
	float2 uv0 = in.texCoord;
	float2 uv1 = in.texCoord;
	
	
	// Foreground effect
	
	// - Foreground shift
	float2 shift = uv0 - 0.5;
	float angle = atan2(shift.y, shift.x);
	uv0 = 0.5 + shift * (1 + 0.1 * cos(10.0 * angle + 2 * time));
	
	float4 src = image0.sample(image0Sampler, uv0);
	
	
	// Background effect
	float2 pixel = 1.0 / float2(image1.get_width(), image1.get_height());
	
	// - Sketch
	// 012
	// 345
	// 678
	float4 dst_pixels[9];
	for (int i = 0; i < 9; ++i) {
		float2 dp = pixel * float2(i % 3 - 1, i / 3 - 1);
		dst_pixels[i] = image1.sample(image1Sampler, uv1 + dp);
	}
	float4 outline = 8 * dst_pixels[4];
	for (int i = 0; i < 9; ++i) {
		if (i == 4) {
			continue;
		}
		outline -= dst_pixels[i];
	}
	float4 dst = dst_pixels[4] * smoothstep(0.8, 1, 1.0 - length(outline));
		
	// - HSV
	float3 hsv = rgb2hsv(dst.rgb);
	float3 col = time_color(uv1, time);
	hsv.x = fract(angle * 0.5 / M_PI_F + 0.5 + rgb2hsv(col).x - 0.25 * time);
	hsv.y = hsv.y * (1.0 - smoothstep(0, 1, length(shift))) + smoothstep(0, 1, length(shift));
	dst.rgb = hsv2rgb(hsv);
	
	return blend(src, dst);
}
